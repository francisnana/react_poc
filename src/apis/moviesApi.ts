import Axios from 'axios';
import {IMoviesContextProps} from "../Models/types";

class MoviesApi {
    private url: string = 'https://api.themoviedb.org/3/discover/movie';

    getAll(key: string): Promise<IMoviesContextProps> {
        return new Promise((resolve, reject) => setTimeout(() => {
            const params = {
                api_key: key,
                primary_release_date: {
                    gte: "2019-12-15",
                    lte: "2020-01-15"
                }
            };
            Axios.get(this.url, {params})
                .then((response) => {
                    resolve(response.data || response);
                }).catch((error) => {
                reject(error);
            })
        }, 5000));

    };
}

export const moviesApi = new MoviesApi();