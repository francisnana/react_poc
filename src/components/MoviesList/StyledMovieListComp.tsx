import styled from 'styled-components';
import {Grid} from "@material-ui/core";

export const StyledMovieListComp =  styled.div`
display: flex;
justify-content:center;
`;

export  const MovieListItem =  styled(Grid)`
height:100%;
padding:16px
`;