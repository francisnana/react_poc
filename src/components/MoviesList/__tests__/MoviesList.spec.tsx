import React from "react";
import {mount, render, shallow} from 'enzyme';
import MoviesList from "../MoviesList";
import toJson from "enzyme-to-json";
import {MoviesContext} from "../../../containers/Movies/MoviesContainer";
import {IMoviesContextProps} from "../../../Models/types";

const mockedMoviesData: IMoviesContextProps = {
     page:1,
     total_results:34,
     total_pages:1,
    results: [{
        popularity: 73,
        vote_count: 5.6,
        video: false,
        poster_path: "string",
        id: 1,
        original_title: "my title",
        genre_ids: [2, 3, 5],
        title: "string",
        vote_average: 6.7,
        overview: "string",
        release_date: "string"
    }, {
         popularity: 73,
         vote_count: 5.6,
         video: false,
         poster_path: "string",
         id: 2,
         original_title: "my title",
         genre_ids: [2, 3, 5],
         title: "string",
         vote_average: 6.7,
         overview: "string",
         release_date: "string"
    }, {
         popularity: 73,
         vote_count: 5.6,
         video: false,
         poster_path: "string",
         id: 3,
         original_title: "my title",
         genre_ids: [2, 3, 5],
         title: "string",
         vote_average: 6.7,
         overview: "string",
         release_date: "string"
    }]
};
const getMoviesContext = (context = mockedMoviesData) => {

    jest.doMock('../../../containers/Movies/MoviesContainer', () => {
        return {
            MoviesContext: {
                Consumer: (props:IMoviesContextProps) =>mockedMoviesData
            }
        }
    });

    // you need to re-require after calling jest.doMock.
    // return the updated LanguageSelector module that now includes the mocked context
    return require('../MoviesList');
};


describe("<MovieList/>", () => {
    beforeEach(() => {
        jest.resetModules();
    });
    it('should render correctly', () => {
        let MoviesListTree = shallow(<MoviesList/>);
        expect(toJson(MoviesListTree)).toMatchSnapshot();
    });
    it('should get the data from the context and render it', ()=>{  //TODO : find a better way of mocking the provider
         const consumerComponentTree = render((
             <MoviesContext.Provider value={mockedMoviesData}>
                  <MoviesList/>
             </MoviesContext.Provider>

         ));
         expect(toJson(consumerComponentTree)).toMatchSnapshot();
         expect( consumerComponentTree.find('.MuiGrid-item').length).toEqual(3);
    });

    // it('should get dynamic data from the context and render it', async ()=>{
    //
    //
    //    //const MyMoviesList = getMoviesContext();
    //
    //
    //     const wrapper = await mount(<MoviesList />, {context:mockedMoviesData});
    //      // const consumerComponentTree = render((
    //      //     <MoviesContext.Provider value={mockedMoviesData}>
    //      //          <MoviesList/>
    //      //     </MoviesContext.Provider>
    //      //
    //      // ));
    //
    //      expect(toJson(wrapper)).toMatchSnapshot();
    //      expect( wrapper.find('.MuiGrid-item').length).toEqual(3);
    // });



});