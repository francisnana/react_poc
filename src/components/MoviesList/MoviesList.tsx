import React, {useContext} from "react";
import {Grid} from "@material-ui/core";
import {IMoviesContextProps, IMoviesDetail} from "../../Models/types";
import {StyledMovieListComp} from "./StyledMovieListComp";
import {MovieItem} from "../index";
import {MoviesContext} from "../../containers/Movies/MoviesContainer";

const getPortion = (array: Array<any> | undefined): Array<any> => {
    return (array) ? array.slice(0, 10) : [];
};
const MoviesList: React.FC<any> = () => {
    const context:IMoviesContextProps | null = useContext(MoviesContext);

    return (
        <StyledMovieListComp>
            <Grid container justify="center" spacing={2} alignItems="stretch">
                {getPortion(context?.results).map((detail: IMoviesDetail) => (
                    <Grid item xs={6} sm={4} style={{display: 'flex'}} key={detail.id}>
                        <MovieItem detail={detail}/>
                    </Grid>
                ))}
            </Grid>
        </StyledMovieListComp>

    );
};

export default MoviesList;

//https://image.tmdb.org/t/p/w185_and_h278_bestv2/30YacPAcxpNemhhwX0PVUl9pVA3.jpg