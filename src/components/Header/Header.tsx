import React from "react";
import {AppBar, IconButton, Toolbar, Typography} from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
const Header = ()=>(
    <AppBar position="static">
        <Toolbar>
            <IconButton edge="start" color="inherit" aria-label="menu">
                    <MenuIcon />
            </IconButton>
            <Typography variant="h6">
                Movies app
            </Typography>
        </Toolbar>
    </AppBar>
);

export  default  Header;
