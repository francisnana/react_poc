import React from "react";

export const Todo = ({description, toggleTodo, completed}:{description:string, toggleTodo:()=>void, completed:boolean})=>(
  <li
  onClick = {toggleTodo}
  >
      {description + ' ..... ' + completed}
  </li>
)