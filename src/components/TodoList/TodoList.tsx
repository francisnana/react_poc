import React from "react";
import {Todo} from "../Todo/Todo";
import {TodoType} from "../../Models/types";

export const TodoList = ({todos, toggleTodo}:{todos:Array<TodoType>, toggleTodo:Function})=>(
    <ul>
      {todos.map(todo =>(
         <Todo
         {...todo}
         toggleTodo={() => { toggleTodo(todo.id)}}
         />

      ))}
    </ul>
)


