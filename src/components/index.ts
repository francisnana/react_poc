import Header from "./Header/Header";
import MoviesList from "./MoviesList/MoviesList";
import MovieItem from "./MovieItem/MovieItem";
import MoviesComponent from "./MoviesComponent/MoviesComponent";

export {
    Header,
    MoviesList,
    MovieItem,
    MoviesComponent
}