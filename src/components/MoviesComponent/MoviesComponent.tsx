import React from "react";
import {CircularProgress} from "@material-ui/core";
import {Container} from "./StyledMoviesComp";
import MoviesList from "../../components/MoviesList/MoviesList";
import {IMoviesContextProps} from "../../Models/types";
import {MoviesContext} from "../../containers/Movies/MoviesContainer";

export interface IMoviesComponentProps {
    loadingState: boolean;
    errorState: {
        hasError: boolean;
        value: string;
    };
    moviesData: IMoviesContextProps;
}

const MoviesComponent: React.FC<any> = ({loadingState, errorState, moviesData}: IMoviesComponentProps) => (
    <Container>
        {
            loadingState ? <CircularProgress/> :
                <div>
                    {
                        errorState.hasError ? <span>{errorState.value}</span> :
                            <MoviesContext.Provider value={moviesData as IMoviesContextProps}>
                                <MoviesList/>
                            </MoviesContext.Provider>
                    }
                </div>

        }
    </Container>
);
export default MoviesComponent
