import React from "react";
import {Avatar, Card, CardContent, CardHeader, Grid, Typography} from "@material-ui/core";
import {IMoviesDetail} from "../../Models/types";
import {MovieCardItem, MovieRatingAvatar} from "./StyledMovieItemComp";

const Moviedetail = ({detail}:{detail:IMoviesDetail})=>{
    return(
    <Card>
        <CardHeader
            avatar={
                <MovieRatingAvatar aria-label="recipe">
                  <Typography variant={"subtitle2"}>  {`${detail.vote_average*10}%` }</Typography>
                </MovieRatingAvatar>
            }
            title={detail.title}
            subheader={detail.original_title}
        />
        {/*<moviePoster*/}
        {/*    image= {`https://image.tmdb.org/t/p/w185_and_h278_bestv2${detail.poster_path}`}*/}
        {/*    title={detail.id}*/}
        {/*/>*/}
        <CardContent>
            <Typography variant="body2" color="textSecondary" component="p">
                {detail.overview}
            </Typography>
        </CardContent>
    </Card>
    );
};

export default Moviedetail;