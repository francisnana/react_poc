import React from "react";
import {Provider} from "react-redux";
import {Route} from "react-router-dom";
import {PlayContainer} from "../containers";
import Home from "../containers/home";
import {Header} from "../components";


const Routes = ({store}: { store: any }) => {
    return (
        <Provider store={store}>
            <div>
              <Header/>
                <Route exact path="/">
                    <Home/>
                </Route>
                <Route path="/play">
                    <PlayContainer/>
                </Route>
            </div>
        </Provider>
    );
};

export default Routes;
