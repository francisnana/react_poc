import {TODOS_ACTIONS, VISIBILITY_ACTIONS} from "../Models/enums";
let nextTodoId = 0
export const addToddo = (description:string) =>({
    type: TODOS_ACTIONS.ADD_TODO,
    id: nextTodoId++,
    description: description
      
})
export const setVisibilityFilter = (filter:boolean) => ({
    type: VISIBILITY_ACTIONS.SET_VISIBILITY_FILTER,
    filter
  })

  export const toggleTodo = (id:number) => {
    return {
      type: TODOS_ACTIONS.TOGGLE_TODO,
      id
    }
  
  }
