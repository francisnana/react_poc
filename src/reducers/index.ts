import { combineReducers } from "redux";
import todos from './reducersList/todos';
import visibilityFilter from './reducersList/visibilityFiter';
import users from './reducersList/users';

const rootReducer = combineReducers({
    todos,
    visibilityFilter, 
    users
});
export default rootReducer;