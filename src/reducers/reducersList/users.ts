import { USERS_ACTIONS } from "../../Models/enums";

const users = (
  state: { name: string; hasTodos: boolean } = { name: "", hasTodos: false },
  action: { type: USERS_ACTIONS; username: string }
) => {
  switch (action.type) {
    case USERS_ACTIONS.SET_LOGIN_DETAILS:
      return {
        name: action.username,
        hasTodos: false
      };
      default:
          return state;
  }
};
export default users;
