import { TODOS_ACTIONS } from "../../Models/enums";

const todos = (state:Array<any> = [], action:{type:TODOS_ACTIONS, id:string, description:string}) => {
    switch(action.type){
        case TODOS_ACTIONS.ADD_TODO :
            return [...state, {
                id: action.id,
                description: action.description,
                completed: false
            }];
        case TODOS_ACTIONS.TOGGLE_TODO:
            return state.map((todo)=>{
               return     (todo.id === action.id)
               ? {...todo, completed: !todo.completed}
               : todo
            });
        default:
        return state;
    }

}
export default todos;