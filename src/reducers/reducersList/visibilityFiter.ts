import { VISIBILITY_FILTERS, VISIBILITY_ACTIONS } from "../../Models/enums";

 const visibilityFilter = (state:VISIBILITY_FILTERS = VISIBILITY_FILTERS.SHOW_ALL, action:{type: string, filter:VISIBILITY_ACTIONS}) => {
 switch(action.type){
     case VISIBILITY_ACTIONS.SET_VISIBILITY_FILTER:
         return action.filter;
         default: 
         return state;
 }
}

export default visibilityFilter;