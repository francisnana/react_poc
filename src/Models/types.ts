export type TodoType = {
  description: string;
  id: number;
  completed: boolean;
};


export interface IMoviesDetail {
  popularity:number;
  vote_count: number;
  video: boolean;
  poster_path: string,
  id: number,
  original_title: string;
  genre_ids: Array<number>;
  title: string;
  vote_average: number;
  overview:string;
  release_date: string;
}

export interface IMoviesContextProps {
  page: number;
  total_results: number;
  total_pages: number;
  results?: Array<IMoviesDetail>;
}
