import {connect} from "react-redux";
import {Button, TextField} from "@material-ui/core";
import * as actions from "../../actions"
import React from "react";

let input:any;

const AddTodo = ({myInput, addTodo}:{myInput:string, addTodo:()=>void}) => {

  return (
    <form onSubmit={addTodo}>
      <TextField
      inputRef={(c) => {
        input = c}}
        required
        id="standard-required"
        label="Required"
        defaultValue=""
        onChange={(event) => {
          myInput =  event.target.value;
          input =event.target.value;
        }}
      />
      <Button type="submit" variant="contained" color="primary">
        Add TODO
      </Button>
    </form>
  );
};
const mapStateToProps = (state:any) => {
return {
    myInput: state.my
}
}
const mapDispatchToProps = (dispatch: any, props: any): any => {
  return {
    addTodo: () => {
      dispatch(actions.addToddo(input));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTodo);
