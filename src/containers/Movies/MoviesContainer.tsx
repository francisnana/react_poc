import React, {useState} from 'react';
import {moviesApi} from '../../apis/moviesApi';
import {IMoviesContextProps} from "../../Models/types";
import {MoviesComponent} from "../../components";
import {IMoviesComponentProps} from "../../components/MoviesComponent/MoviesComponent";

const key = "ce3243ed1b3701b007642ee0f80a9520";


export const MoviesContext = React.createContext<IMoviesContextProps | null>(null);

const MoviesContainer: React.FC<any> = () => {
    let [moviesData, setMovies] = useState({
        page: 0,
        total_results: 0,
        total_pages: 0
    });

    const [errorState, setErrorState] = useState({
        hasError: false,
        value: ''
    });
    const [loadingState, setLoadingState] = useState(true);
    React.useEffect(() => {
        moviesApi.getAll(key).then((data) => {
            setLoadingState(false);
            setMovies(data);

        }).catch((error) => {
            setErrorState({hasError: true, value: error});
            setLoadingState(false);
        })
    }, []);
    let props: IMoviesComponentProps = {errorState, loadingState, moviesData};
    return (
        <MoviesComponent {...props}/>
    );
};

export default MoviesContainer;

